import { Component, OnInit } from '@angular/core';
import { Game } from '../shared/game.model';
import { GamesService } from '../shared/games.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit {
  game!: Game;

  constructor(private route: ActivatedRoute, private gamesService: GamesService) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const gameId = parseInt(params['id']);
      this.game = this.gamesService.getGame(gameId);
    })

  }

}
