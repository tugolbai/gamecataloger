import { Component, OnInit } from '@angular/core';
import { Game } from '../shared/game.model';
import { GamesService } from '../shared/games.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  games!: Game[];

  constructor(private gameService: GamesService) { }

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.gamesChange.subscribe((games: Game[]) => {
      this.games = games;
    });
  }
}
