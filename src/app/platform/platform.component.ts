import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GamesService } from '../shared/games.service';
import { Game } from '../shared/game.model';

@Component({
  selector: 'app-platform',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.css']
})
export class PlatformComponent implements OnInit {
  @ViewChild('nameLi') nameLi!: ElementRef;
  platforms = ['action', 'simulation', 'arcade'];
  platform: Game[] = [];

  constructor(private gamesService: GamesService) { }

  onPlatform() {
    const name = this.nameLi.nativeElement.innerText;
    this.platform = this.gamesService.getGamesByPlatorm(name);
    console.log(this.platform);
    console.log(name);
  }

  ngOnInit(): void {

  }

}
