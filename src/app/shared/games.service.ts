import { EventEmitter } from '@angular/core';
import { Game } from './game.model';

export class GamesService {
  gamesChange = new EventEmitter<Game[]>();

  private games: Game[] = [
    new Game('Mortal Kombat (2011)', 'https://cdn.kanobu.ru/c/3844d9db62137778d71b004897704b4d/400x510' +
      '/cdn.kanobu.ru/games/48/f0a421ed76ee46ae897b26554d7fdd96', 'arcade', 'Mortal Kombat IX —' +
      ' видеоигра в жанре файтинг, девятая часть в серии Mortal Kombat, разработанная компанией NetherRealm Studios. ' +
      'События игры происходят после окончания Mortal Kombat: Armageddon. После противостояния Шао Кан и Рейдена ' +
      'Император побеждает, и Рейден едва успевает послать сообщение себе в прошое, пока Шао Кан его не добил. ' +
      'Рейден из прошлого, получив послание, меняет сюжетную линию вообще всего сериала, так как действует ' +
      'совершенно не так, как действовал ранее. В игре появляются многие бойцы, которых убили ранее. Райдену же ' +
      'предстоит изменить то мрачное будущее, которое показывает ему медальон.'),
    new Game('Forza Horizon 4', 'https://cdn.igromania.ru/mnt/game_screenshots_gallery/7/7/6/e/5/661890' +
      '/b4d6af101d245642_320x180.jpg', 'arcade', 'Времена года полностью меняют облик главного ' +
      'автомобильного фестиваля планеты. Исследуйте чудесные пейзажи и исторические достопримечательности' +
      ' Великобритании, путешествуя по открытому миру в одиночку или вместе с другими игроками. Собирайте, ' +
      'модифицируйте и испытывайте более 450 автомобилей. Участвуйте в гонках, выполняйте трюки, творите и исследуйте,' +
      ' чтобы стать суперзвездой Horizon. В цифровое стандартное издание Forza Horizon 4 входит полная версия игры ' +
      'Forza Horizon 4 и набор машин Formula Drift.'),
    new Game('Portal 2', 'https://cdn.igromania.ru/mnt/games/d/c/d/2/f/3/9404/0db69888db4ccc49' +
      '_320x180.jpg', 'arcade', 'Вторая часть Portal - идеальная игра для любителей ностальгии.' +
      ' Та же самая главгероиня, противная система GLaDOS и привычные комнаты лаборатории Aperture Science. Но не ' +
      'обошлось и без новинок - загадки будут более сложными и хитроумными, а часть сюжета пройдет за пределами ' +
      'научного комплекса.'),
    new Game('Grand Theft Auto V (обновленная версия)', 'https://metarankings.ru/wp-content/uploads/' +
      'Grand-Theft-Auto-V-new-cover-100x140.png', 'action', 'Игра обзавелась похорошевшей графикой,' +
      ' увеличенным разрешением, новыми деталями, большей дальностью прорисовки, всеми дополнениями и видом от первого' +
      ' лица. Теперь игроки могут исследовать мир Лос-Сантоса глазами своего персонажа от первого лица, открывая ' +
      'детали мира Grand Theft Auto V совершенно по новому.'),
    new Game('God of War (2018)', 'https://metarankings.ru/wp-content/uploads/God-of-War-boxart-' +
      'cover-100x140.jpg', 'action', 'God of War для PS4 — это перезапуск легендарной брутальной ' +
      'франшизы от Sony Santa Monica, который расскажет совершенно новую эмоциональную историю о путешествии Кратоса ' +
      'и даст игрокам переосмысленный геймплей с видом от третьего лица. Вы станете свидетелями убедительной драмы, ' +
      'которая разворачивается, когда бессмертные полубоги принимают решения о своей перемене.'),
    new Game('The Last of Us: Remastered', 'https://metarankings.ru/wp-content/uploads/The-Last-of-Us-' +
      'Remastered-cover-100x140.png', 'action', 'Это переиздание лучшей игры 2013 для PlayStation 3' +
      ' — The Last of Us, которое будет иметь более большее разрешение 1080p, модели более высокого разрешения,' +
      ' новые тени и освещение, обновленные текстуры и множество других улучшений.'),
    new Game('Minecraft', 'https://s3.gaming-cdn.com/images/products/5657/orig/minecraft-windows-10-' +
      'edition-windows-10-edition-pc-game-microsoft-store-cover.jpg', 'simulation', 'Minecraft —' +
      ' компьютерная инди-игра в жанре песочницы, созданная шведским программистом Маркусом Перссоном и выпущенная ' +
      'его компанией Mojang AB. Перссон опубликовал начальную версию игры в 2009 году; в конце 2011 года была выпущена' +
      ' стабильная версия для ПК Microsoft Windows, распространявшаяся через официальный сайт'),
    new Game('Euro Truck Simulator 2', 'https://upload.wikimedia.org/wikipedia/ru/1/18/Euro' +
      '_Truck_Simulator_2.jpg', 'simulation', 'Euro Truck Simulator 2 — компьютерная игра в жанре ' +
      'симулятора водителя-дальнобойщика с элементами экономической стратегии. Разработана и выпущена чешской ' +
      'компанией SCS Software в 2012 году. Часть серии игр Truck Simulator'),
    new Game('FIFA 22', 'https://upload.wikimedia.org/wikipedia/ru/6/6c/FIFA_22_Cover.jpg', 'sim' +
      'ulation', 'FIFA 22 — 29-ая по счёту компьютерная игра из серии FIFA в жанре футбольного симулятора,' +
      ' разработанная компаниями EA Vancouver под издательством Electronic Arts. На ПК, PlayStation 4, PlayStation 5,' +
      ' Xbox One, Xbox Series X/S и Nintendo Switch игра уже вышла в октябре 2021 года.'),
  ];

  getGames() {
    return this.games.slice();
  }

  getGame(index: number) {
    return this.games[index];
  }

  getGamesByPlatorm(platform: string) {
    return this.games.filter(platforms => platforms.platform === platform);
  }

  addGame(game: Game) {
    this.games.push(game);
    this.gamesChange.emit(this.games);
  }
}
