import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PlatformComponent } from './platform/platform.component';
import { NewGameComponent } from './new-game/new-game.component';
import { GameDetailComponent } from './game-detail/game-detail.component';

const routes: Routes = [
  {path: '', component: HomeComponent, children: [
      {path: ':id', component: GameDetailComponent},
    ]},
  {path: 'platform/game', component: PlatformComponent, children: [
      {path: ':name', component: GameDetailComponent  },
    ]},
  {path: 'new-game/game', component: NewGameComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
